var Node = Backbone.Model.extend({
	urlRoot: 'http://cn-api.sinsuwan.com/nodes',
	intitialize: function(){
		//var that = this;
	}
});

var Nodes = Backbone.Collection.extend({
	model : Node,
	initialize : function(){
		var _self = this;
	}
});

var AppView = Backbone.View.extend({

	el : $("#ext-hover-box"),
	isParent : false,
	parentId : null,
	fullDescription : "",
	vis : null,
	cluster : {
		name : "",//$(e.target).html(), 
		children : [] // { name : "name",  children : [] }
	},

	event : {
		"click .ext_tag" : "mapBox"
	},

	initialize : function(){
		var _self = this;
		$(document).on("keyup", function(e) { _self.escape(e);});
		$(document).on("mouseup dblclick", function(e) { _self.tagBox(e);});
		console.log("AppView is initialized...");
		if(this.parentId == null) {
			this.parentalStatus();
			if(this.isParent) {
				this.getParent();
			}
		}
		// kill all newline characters so we don't have to deal with it later on
		_self.stripNewline();
	},

	stripNewline : function(){
		var _self = this;
		$('p').each(function(){
			_p = $(this);
			if(_p.children().length == 0){
				_p.html(function(index, oldhtml){
					str = "";
					$(oldhtml.trim().split('\n')).each(function(){
						str += this.trim().replace(/\s{2,}/, " ");
					});
					return str;
				});
			}
		});
		_self.claimBullshit();
		_self.getTagToHighlight();
	},
	
	render : function(){
		// var _self = this;
		// if(_self.isParent){
		// 	_self.highlightTags();
		// }
	},
	
	escape : function(e) {
		if(e.keyCode === 27) {
			$('#ext-hover-box').fadeOut().html('');
			$('#searchResultsBox').fadeOut().html();
		}
	},

	clusterGetFirstLevelTags : function(response) {
		var children = [];
		var _self = this;
		response.forEach(function(res) {
			temp = {name : res.Node.name};
			temp["children"] = _self.clusterGetSecondLevelTags(res);
			children.push(temp);
		});
		return children;
	},

	clusterGetSecondLevelTags : function(response) {
		var _self = this;
		var temp = [];
		$.ajax({
			url : "http://cn-api.sinsuwan.com/nodes/",
			data : { name : response.Node.name, parent_node_id : _self.parentId },
			dataType : 'json',
			async : false,
			success : function(response) {
				_self.clusterGetChildrenParents(response, temp);
			}	
		});
		return temp;
	},

	clusterGetChildrenParents : function(response, array_t) {
		var temp = [];//, temp_test_names = [], temp_test_ids = [], temp_test_result = [];
		response.forEach(function(res) {
			$.ajax({
				url : "http://cn-api.sinsuwan.com/nodes/",
				data : { parent_node_id : res.ParentNode.id },
				dataType : 'json',
				async : false,
				success : function(response) {
					response.forEach(function(tag) {
						if(res.Node.name != tag.Node.name) {
							temp.push(tag.Node.name);
							//temp_test_names.push(tag.Node.name);
							//temp_test_ids.push(tag.Node.id);
						}
					});
				}		
			});
		});
		// for (var i = temp_test_names.length - 1; i >= 0; i--) {
		// 	v = {temp_test_names[i] : temp_test_ids[i]};
		// 	console.log(temp_test_names[i], temp_test_ids[i]);
		// };
		//console.log(temp_test_result);
		temp = _.uniq(temp);
		temp.forEach(function(a){
			array_t.push({name: a});
		});
	},

	mapBox : function(e){
		e.stopPropagation();
		var _self = this;
		$('#ext-hover-box').empty();
		_self.fullDescription = $(e.target).html();
		$.ajax({
			url : "http://cn-api.sinsuwan.com/nodes/",
			data : { description: $(e.target).html() },
			dataType : 'json',
			async : false,
			success : function(response) {
				_self.cluster["name"] = $(e.target).html();
				tmp_children = _self.clusterGetFirstLevelTags(response);
				if(tmp_children != 0){
					_self.cluster["children"] = tmp_children;
				}
			}
		});

		$("#ext-hover-box").on('mouseup dblclick', function(e){ return false; });
		$("#ext-hover-box").css({top : e.pageY + "px", left : e.pageX + "px"});
		$("#ext-hover-box").addClass("map_box");
		$("#ext-hover-box").append( $("<div />").html('Add tag : <input type="text" id="addtaginput" /><input id="addtag" type="submit" value="Submit" />') );
		$("#ext-hover-box").fadeIn();
		$("#addtag").on("click", function(e) { _self.postTag($("#addtaginput").val(), _self.fullDescription); });
		var width = 450,
		    height = 450;

		var t_cluster = d3.layout.cluster()
		    .size([height, width - 285]);

		var diagonal = d3.svg.diagonal()
		    .projection(function(d) { return [d.y, d.x]; });
		
		_self.vis = d3.select("#ext-hover-box")
			.style("width", width+"px")
			.append("svg")
		    .attr("width", width)
		    .attr("height", height)
		  	.append("g")
		    .attr("transform", "translate("+ ((width/2)-90) +", 0)");

		var nodes = t_cluster.nodes(this.cluster);

		var link = _self.vis.selectAll("path.link")
		  	.data(t_cluster.links(nodes))
			.enter().append("path")
		  	.attr("class", "link")
		  	.attr("d", diagonal);

		var node = _self.vis.selectAll("g.node")
			.data(nodes)
			.enter().append("g")
			.attr("class", "node")
			.attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

		node.append("circle")
		  	.attr("r", 4.5);

		var text = node.append("text")
			.attr("dx", function(d) { return d.children ? -8 : 8; })
			.attr("dy", 3)
			.attr("text-anchor", function(d) { return d.children ? "end" : "start"; })
			.text(function(d) { 
				d.namearray = []; 
				_self.arrangeString(d.name, d.namearray); 
				return (d.namearray.length == 0)?d.name:""; 
			})
			.on("click", _self.searchResultsBox);

		var tspans = text.selectAll("tspan.txt_line")
			.data(function(d){return d.namearray;})
			.enter().append("tspan")
			.attr("class", "txt_line")
			.attr("x", 0)
			.attr("dy", 10)
			.text(function(d){
				return d;
			});

		tspans.forEach(function(ts, idx){
			ts.forEach(function(it, idx2){	
				if(idx2 == 0){
					$(it).removeAttr("dy");
				}	
			});
		});
	},

	arrangeString : function(string, array){
		t_arr = string.split(' ');
		if(t_arr.length < 3){
			array = t_arr;
		}
		else{
			k = 0;
			for(i = 0; i < t_arr.length; i+=3){
				t_str = "";
				for(j = 0; (j < 3 && j+i < t_arr.length); j++){
					t_str += t_arr[i+j] + " ";
				}
				array[k] = t_str;
				k++;
			}
		}
	},

	searchResultsBox : function(d) {
		var getPreview = function(page, darum){
			var result_string = "";
			$.ajax({
				url : 'http://cn-api.sinsuwan.com/nodes/',
				data : {description: page},
				dataType : "json",
				async : false,
				success : function(res){
					res[0].ChildNode.forEach(function(item){
						if(item.name == darum.name || item.name == darum.parent.name){
							result_string += item.description + "(" + item.name + ")" + "...";
						}
					});
				}
			});
			return result_string;
		}

		d3.event.stopPropagation();

		$('#searchResultsBox').fadeOut();
		if(!d.children){
			if(!$('#searchResultsBox').length) {
				$('body').append( $("<div />").attr('id', 'searchResultsBox') );
			}
			$("#searchResultsBox").on('click', function(e){ return false; });
			$('#searchResultsBox').empty();
			$('#searchResultsBox').css({top : d3.event.pageY+ "px", left : d3.event.pageX+ "px"});
			$.ajax({
				url : 'http://cn-api.sinsuwan.com/nodes/',
				dataType : 'json',
				data : { names : [ d.name, d.parent.name ] }, 
				async : false,
				success : function(data) {
					$('#searchResultsBox').append( $('<ul />').attr('id', 'list') );
					for(res in data) {
						$('#list')
							.append( $('<li />')
								.append( $('<a />').attr('href', data[res].nodes.description).append(data[res].nodes.description) ) 
								.append( $('<p />')
									.append(getPreview(data[res].nodes.description, d)))
							);
					}
				}
			});
			$('#searchResultsBox').fadeIn(); 
		}
	},

	getSelectedText : function() {
		// we don't need to detect other kind of browser at the moment. it's a chrome extension.
		var text = window.getSelection(); 
		// detect if it's a Range or do nothing
		if(text.type == "Caret"){
			return false;
		}
		// set fullDescription to be ready to post to server
		this.fullDescription = text.toString();
		// lets not truncate the text for now
		// text = this.truncate(text.toString()).replace(/^\s+|\s+$/g, '');
		return  text;
	},
	
	tagBox : function(e) {
		var str = this.getSelectedText();
		var _self = this;
		$("#ext-hover-box").removeClass("map_box");
		if(str != false && str != "" && str != undefined && str.length != 0){
			$("#ext-hover-box").on('mouseup dblclick', function(e){ return false; });
			$("#ext-hover-box").css({top : e.pageY+ "px", left : e.pageX+ "px"});
			$("#ext-hover-box").html('<p>'+str+'</p>Tag it : <input type="text" id="addtaginput" /><input id="addtag" type="submit" value="Submit" />').fadeIn();
			//$("#addtaginput").trigger('focus');
			$("#addtag").on("click", function(e) { _self.postTag($("#addtaginput").val(), _self.fullDescription); });
			$("ext-hover-box").fadeIn();
		}else{
			$("#ext-hover-box").empty().fadeOut();
			$("#searchResultsBox").empty().fadeOut();
		}
	},

	getParent : function() { 
		var _self = this;
		$.ajax({
			url : 'http://cn-api.sinsuwan.com/nodes/getparent',
			dataType : 'json',
			data : { url : document.URL }, 
			async : false,
			success : function(data) {
				_self.parentId = data.Node.id;	
			}
		});
		console.log('parentid', _self.parentId);
	},

	postTag : function(name, description) {
		if(!this.isParent) {
			this.getParent();
		}
		data = { 
			"data[Node][name]" : name, 
			"data[Node][type_id]" : "4ffe6809-2dc0-4a96-885e-87a9ae60b218", 
			"data[Node][description]" : description,
			"data[Node][parent_node_id]" : this.parentId
		};
		//console.log(data);
		// we can animate fadeout or show error if there is one later
		$.post('http://cn-api.sinsuwan.com/nodes/add', data, function(res){
			console.log(res);
		});
		$("#ext-hover-box").empty().fadeOut();
		this.getTagToHighlight();
	},

	parentalStatus : function() {
		var _self = this;
		$.ajax({
			url : 'http://cn-api.sinsuwan.com/nodes/parentalstatus',
			dataType : 'json',
			data : { url : document.URL }, 
			async : false,
			success : function(data) {
				_self.isParent = data;
			}
		});
	},

	highlightTag : function(tag, target_ele){
		try {	
			$(target_ele).html(function(index, oldhtml){
				var patt = new RegExp(tag.Node.description, "igm"); 
				return oldhtml.replace(patt, "<span class='ext_tag' node_id='"+ tag.Node.id +"'>" + tag.Node.description + "</span>");
			});
		}
		catch (errors){
			console.log("error : ", errors, "target_ele : ", target_ele, "tag : ", tag);
		}
	},

	getTagToHighlight : function(){
		var _self = this;
		data = {parent_node_id : _self.parentId};
		$.getJSON('http://cn-api.sinsuwan.com/nodes', data, function(response){
			response.forEach(function(tag){
				array_ele = $('*:contains("'+tag.Node.description+'")');
				$(array_ele).each(function(){
					if (_self.shouldBeHighlight($(this).get(0).tagName) && !$(this).hasClass("ext_tag") ){ //&& $(this).children().length == 0) {
						_self.highlightTag(tag, this);
					}
				});
			});
			$('.ext_tag').on("click", function(e) { _self.mapBox(e); });
		});
	},

	claimBullshit : function() { 
		var claims = $('*[id^=claim_item]');
		for(i = 0; i < claims.length; i++) {
			var text = $(claims[i]).children().last().html(function(index, oldhtml) {
				return oldhtml.replace(/\<[/]*claim-text>\b/gim, '');
			});
		}
	},

	shouldBeHighlight : function(ele_type){
		prohibited_tagname = {
			"HTML" : true,
			"BODY" : true,
			"SCRIPT": true,
			"PRE": true,
			"A" : true,
			"HEAD": true,
			"STYLE": true,
			"LINK": true,
			"TITLE": true
		};
		return !prohibited_tagname[ele_type];
	}

});
var App = new AppView();